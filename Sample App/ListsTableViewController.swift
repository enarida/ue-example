//
//  ListsTableViewController.swift
//  Sample App
//
//  Created by Elisha Narida on 14/09/2017.
//  Copyright © 2017 Elisha Narida. All rights reserved.
//

import Foundation
import UIKit

class ListTableViewController: UITableViewController {
    
    let emojisArray = ["😂","👶","🤡","💯","👴🏿","👪"]
    let cellIdentifier = "Cell"
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return emojisArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        
        let emoji = emojisArray[indexPath.row]
        cell.textLabel?.text = emoji
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let emoji = emojisArray[indexPath.row]
        let message = "\(emoji) tapped"
        let alert = UIAlertController(title: "Tapped detected", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
