//
//  FirstViewController.swift
//  Sample App
//
//  Created by Elisha Narida on 14/09/2017.
//  Copyright © 2017 Elisha Narida. All rights reserved.
//

import Foundation
import UIKit

class FirstViewController: UIViewController {
    
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBAction func buttonTapped(_ sender: Any) {
        
        let alertMessage = "Hello \(nameTextField.text!) welcome to UE Caloocan"
        
        let alert = UIAlertController(title: "Register Sucess", message: alertMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Done", style: .default, handler: nil))
        
        present(alert, animated: true, completion: nil)
        
    }
}
